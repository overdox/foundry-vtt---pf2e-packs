# foundry-VTT - pf2e-packs

This is the Lost Omens SRD packs for Pathfinder Second Edition to Foundry VTT. 

Included in these compendiums you will find

* Lost Omens Character Guide feats and spells
* Lost Omens Gods & Magic feats and spells
* Lost Omens Wold Guide feats and spells
